﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD48
{
	public class PowderApplier : MonoBehaviour
	{
		protected int powderPhysKernel = 0;
		protected int powderTextureID = 0;
		protected int powderResolutionID = 0;
		protected int powderSpawnPosID = 0;
		protected int powderSpawnColor = 0;
		protected int resolution = 512;
		protected RenderTexture texture = null;

		protected int selectedPowderCol = 0;

		[SerializeField]
		private MeshRenderer _renderer;
		protected MeshRenderer Renderer
		{
			get { return _renderer ?? (_renderer = GetComponent<MeshRenderer>()); }
		}

		[SerializeField]
		private ComputeShader _powderPhysics;
		public ComputeShader PowderPhysics => _powderPhysics;

		[SerializeField]
		private Camera _cam;
		public Camera cam 
		{
			get { return _cam ?? (_cam = GetComponent<Camera>()); }
		}

		[SerializeField]
		private ComputeShader _initializationShader;

		/// <summary>
		/// Called when the script is intantiated
		/// </summary>
		private void Start()
		{
			// find the entry point for CSMain in the compute shader and the shader property IDs
			powderPhysKernel = PowderPhysics.FindKernel("CSMain");
			powderTextureID = Shader.PropertyToID("result");
			powderResolutionID = Shader.PropertyToID("resolution");
			powderSpawnPosID = Shader.PropertyToID("spawnPos");
			powderSpawnColor = Shader.PropertyToID("spawnCol");

			// create the render texture to set to the mesh renderer
			texture = new RenderTexture(resolution, resolution, 24);
			texture.enableRandomWrite = true;
			texture.filterMode = FilterMode.Point;
			texture.Create();

			// populate the texture with some random pixels
			InitializeTexture();

			// set the properties in the compute shader
			PowderPhysics.SetTexture(0, powderTextureID, texture);
			PowderPhysics.SetInt(powderResolutionID, resolution);
			PowderPhysics.SetFloats(powderSpawnColor, 0.5f, 0.5f, 0.5f, 0.5f);

			// apply the texture to the mesh renderer
			Renderer.material.mainTexture = texture;
		}

		/// <summary>
		/// called each frame()
		/// </summary>
		private void Update()
		{
			// shift the selected input color based on mouse scroll
			if (Input.mouseScrollDelta.y != 0)
			{
				int dir = (int)Mathf.Sign(Input.mouseScrollDelta.y);
				selectedPowderCol += dir;
			}
		}

		/// <summary>
		/// Called 50 times per second
		/// </summary>
		private void FixedUpdate()
		{
			SetPowderSpawnVars();
			DoPowderPhysicsStep();
		}

		/// <summary>
		/// called when camera renders
		/// </summary>
		/// <param name="source"> the camera generated image </param>
		/// <param name="destination"> the image that is displayed on screen </param>
		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			Graphics.Blit(texture, destination);
		}

		/// <summary>
		/// places random powder within the texture
		/// </summary>
		private void InitializeTexture()
		{
			int kernel = _initializationShader.FindKernel("CSMain");
			_initializationShader.SetTexture(kernel, "result", texture);
			_initializationShader.Dispatch(kernel, resolution / 8, resolution / 8, 1);
		}

		/// <summary>
		/// sets the powder spawn position to the mouse position
		/// </summary>
		private void SetPowderSpawnVars()
		{
			// if mouse button is not down, don't spawn powder
			if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
			{
				PowderPhysics.SetInts(powderSpawnPosID, -1000, -1000);
				return;
			}

			// get input color to write to texture at mouse pos
			Vector4 inCol = GetInputColor();

			// pass input color to compute shader
			PowderPhysics.SetFloats(powderSpawnColor, inCol.x, inCol.y, inCol.z, inCol.w);

			// get mouse position
			Vector3 mouse = Input.mousePosition;

			// converte mouse pos to normalized screen pos
			mouse = cam.ScreenToViewportPoint(mouse);

			// convert mouse to int position multiplied by the texture resolution
			Vector2Int pos = new Vector2Int(
				Mathf.FloorToInt(mouse.x * texture.width), 
				Mathf.FloorToInt(mouse.y * texture.height)
			);

			// pass position to compute shader
			PowderPhysics.SetInts(powderSpawnPosID, pos.x, pos.y);
		}

		/// <summary>
		/// returns the color that should be output at the mouse coordinates
		/// </summary>
		private Vector4 GetInputColor()
		{
			// drop powder on left click
			if (Input.GetMouseButton(0))
			{
				switch (Mathf.Abs(selectedPowderCol) % 2)
				{
					case 0: // powder
						return new Vector4(0.5f, 0.5f, 0.5f, 0.5f);

					case 1: // solid
						return new Vector4(0, 0, 0, 1);
				}
			}

			// erase on right click
			else if(Input.GetMouseButton(1))
			{
				return new Vector4(1, 1, 1, 0);
			}

			// if no color specified
			return new Vector4();
		}

		/// <summary>
		/// iterates through each pixel in the texture and does a single powder physics step
		/// </summary>
		private void DoPowderPhysicsStep()
		{
			PowderPhysics.Dispatch(powderPhysKernel, resolution / 16, resolution / 16, 1);
		}
	}
}